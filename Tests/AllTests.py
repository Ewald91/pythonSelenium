import unittest
from HtmlTestRunner import HTMLTestRunner
from selenium import webdriver

import os, sys, inspect

# fetch path to the directory in which current file is, from root directory or C:\ (or whatever driver number it is)
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
# extract the path to parent directory
parentdir = os.path.dirname(currentdir)
# insert path to the folder from parent directory from which the python module/ file is to be imported
sys.path.insert(0, parentdir+'\Resources')
sys.path.insert(0, parentdir+'\Resources\PageObjects')

from TestData import TestData
from Pages import HomePage


class Test_BaseClass(unittest.TestCase):

    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        self.driver = webdriver.Chrome(TestData.CHROME_EXECUTABLE_PATH, options=chrome_options)
        self.driver.maximize_window()

    def tearDown(self):
        self.driver.close()
        self.driver.quit()


class Test_Suite(Test_BaseClass):
    def setUp(self):
        super().setUp()

    def test_login(self):
        self.homepage = HomePage(self.driver)
        self.homepage.go_to_login()
        self.homepage.login()


if __name__ == '__main__':
    # specify path where the HTML reports for testcase execution are to be generated
    unittest.main(testRunner=HTMLTestRunner(output=parentdir + '\Reports'))