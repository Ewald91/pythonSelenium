## Project purpose

This projects aim is to supply you with a working project-shell (quick-start) that is effeciently 
setup to save you a lot of time with the initial start of a python-selenium project. 

Also it's easily extendible if you'd like to add other frameworks to your project like 'behave' 
for BDD.

For this project we've created a basic and robust structure wich contains all the necessary 
components and recourses

## Project structure

In the root of the project you'll find 4 main folders. 

#### Webdrivers

This folder should contain the webdrivers of the browser you would like to use. After cloning 
you'll find only the chromedriver (version 76) in there. Make sure to run the correct 
browser version with the webdriver.

#### Reports

The testrunner is configured to output the HTML reporting files to this directory by the line
below:

    unittest.main(testRunner=HTMLTestRunner(output=parentdir + '\Reports'))`

#### Tests

This folder contains one file (named `AllTests.py`) that contains the different testclasses 
that could contain multiple testcases. 

#### Resources

This folder contains the backbone of this setup. First have the 'TestData.py' file wich is 
nothing more than some basic settings. 

In the subfolder called 'PageObjects' we've setup 2 files. 

First we have the locators.py, wich contains locators (no way!..) that result into elements. 

Second we have the Pages.py wich are the teststeps that rely on the settings (TestData.py)
and the locators. 

## Usage

### Add more tests

1. To add more tests you first add more steps (`Page.py`). Make your code readable by putting 
the locators not in the teststeps, but in in the `Locators.py`.

2. Then setup the steps in the correct order in a method (=testcase) in testClass (=testsuite) in the 
`AllTests.py` file.


### Run your tests

You can easily run your tests in the commandline by letting Python execute the `AllTests.py` file

We've set this file up so that unittest will execute all the tests secified.

Run with the following command from the Tests folder:
`python AllTests.py -v `

