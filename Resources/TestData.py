import os

class TestData(object):

    DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    CHROME_EXECUTABLE_PATH = os.path.join(DIR + '/webdrivers/chromedriver.exe').replace("\\","/")
    URL = 'https://stackoverflow.com'