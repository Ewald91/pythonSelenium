import time, os, sys, inspect
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
# fetch path to the directory in which current file is, from root directory or C:\ (or whatever driver number it is)
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
# extract the path to parent directory
parentdir = os.path.dirname(currentdir)
# insert path to the folder from parent directory from which the python module/ file is to be imported
sys.path.insert(0, parentdir)
from Locators import Locators
from TestData import TestData


class BaseActions():

    def __init__(self, driver):
        self.driver = driver

    def click(self, locator):
        WebDriverWait(self.driver, 10)\
            .until(EC.visibility_of_element_located(locator)).click()

    def enter_text(self, by_locator, text):
        return WebDriverWait(self.driver, 10)\
            .until(EC.visibility_of_element_located(by_locator)).send_keys(text)

    def is_enabled(self, by_locator):
        return WebDriverWait(self.driver, 10)\
            .until(EC.visibility_of_element_located(by_locator))

    def is_visible(self, by_locator):
        element = WebDriverWait(self.driver, 10)\
            .until(EC.visibility_of_element_located(by_locator))
        return bool(element)

    def hover_to(self, by_locator):
        element = WebDriverWait(self.driver, 10)\
            .until(EC.visibility_of_element_located(by_locator))
        ActionChains(self.driver).move_to_element(element).perform()

    def assert_text(self, by_locator, text):
        web_element = WebDriverWait(self.driver, 10)\
            .until(EC.visibility_of_element_located(by_locator))
        assert web_element.text == text


class HomePage(BaseActions):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver.get(TestData.URL)

    def go_to_login(self):
        self.click(Locators.LOGIN_BUTTON)

    def login(self):
        self.enter_text(Locators.USERNAME_INPUT,os.environ.get('SELENIUM_USR'))
        self.enter_text(Locators.PASSWORD_INPUT,os.environ.get('SELENIUM_PASS'))
        self.click(Locators.SUBMIT_BUTTON)
        time.sleep(5)
