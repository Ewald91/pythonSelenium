from selenium.webdriver.common.by import By

class Locators():
    # --- HomePage ---
    LOGIN_BUTTON = (By.XPATH,"/html/body/header/div/ol[2]/li[2]/a[1]")

    # --- LoginPage ---
    USERNAME_INPUT = (By.ID, 'email')
    PASSWORD_INPUT = (By.ID, 'password')
    SUBMIT_BUTTON = (By.ID, 'submit-button')